# Custom PC Shop GUI
## Setting up Eclipse Project
1. Right-click the blank space in the Package Explorer
    * Go to 'Import...' > 'Git' > 'Projects from Git'
    * Select 'Clone URI'
    * Add this project's URL (either HTTPS or SSL) to the URI input field
    * Click 'Next', then 'Next' to choose master branch
    * Choose the default project location directory, then click 'Next'
    * Choose 'Import as general project'
    * Click 'Next', and finally 'Finish'
2. Right-click the newly created project and choose 'Configure' > 'Convert to Maven project'

The project should now be ready to test. There is a single Unit test named `FetchPsuTest`. It simply fetches all PSU 
parts from the database, verifies that > 0 results returned and prints the PSUs to the console.

## Running the Test
1. Make sure you have the database container up and running. Instructions can be found at https://gitlab.com/cpsc5021-team5-Fall-2018/custom-pc-shop
2. Right-click `FetchPsuTest` and choose 'Run as' > 'JUnit Test'

## GUI Query Example

### Java class: src/main/java:
* `list_psus.ListPsus`

### Resources: src/main/resources/list_psus:
* `list_psus.fxml`
* `ListPsus.css`