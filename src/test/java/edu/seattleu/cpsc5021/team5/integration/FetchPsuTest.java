package edu.seattleu.cpsc5021.team5.integration;

import edu.seattleu.cpsc5021.team5.part.psu.PSU;
import edu.seattleu.cpsc5021.team5.part.psu.PsuDao;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertNotEquals;

public class FetchPsuTest {
    private PsuDao psuDao;

    @Before
    public void setup() {
        psuDao = new PsuDao();
    }

    @Test
    public void testFetchAllPsus() {
        List<PSU> allPsus = psuDao.fetchAll();
        assertNotEquals(0, allPsus.size());

        for (PSU psu: allPsus)
            System.out.println(psu);
    }

}
