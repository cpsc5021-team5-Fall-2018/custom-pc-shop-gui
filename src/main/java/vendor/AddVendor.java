package vendor;


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class AddVendor extends Application{

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("add_vendor.fxml"));

        stage.setTitle("Add Vendor");
        stage.setScene(new Scene(root, 1024, 960));
        stage.show();
    }

    public static void main(String[] args) {
        Application.launch(AddVendor.class, args);
    }
}
