package edu.seattleu.cpsc5021.team5;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class AppConfig {

    private static final String APPLICATION_PROPERTIES_LOCATION = "application.properties";

    private static Properties properties;

    private static void loadProperties() {
        properties = new Properties();
        try (
                InputStream applicationResource = AppConfig.class.getClassLoader().getResourceAsStream(APPLICATION_PROPERTIES_LOCATION);
                ) {
            properties.load(applicationResource);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Properties getProperties() {
        if (properties == null)
            loadProperties();

        return new Properties(properties);
    }

}
