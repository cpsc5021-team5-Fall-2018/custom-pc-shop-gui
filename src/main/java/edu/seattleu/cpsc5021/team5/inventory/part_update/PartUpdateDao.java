package edu.seattleu.cpsc5021.team5.inventory.part_update;

import edu.seattleu.cpsc5021.team5.AppConfig;
import edu.seattleu.cpsc5021.team5.DbConfig;

import java.sql.*;
import java.util.Properties;

public class PartUpdateDao {
    private static final String UPDATE_PART_IN_INVENTORY_KEY = "sql.inventory.part-update";
    private Properties appConfig = AppConfig.getProperties();

    private DbConfig dbConfig = DbConfig.getInstance();

    public boolean updateinventorysupply(Integer partID, Integer partAmount) {
        boolean success;

        try (
                Connection cxn = dbConfig.getDataSource().getConnection();
        ) {
            String callUpdateInventoryStoredProcedure = appConfig.getProperty(UPDATE_PART_IN_INVENTORY_KEY);
            CallableStatement stmt = cxn.prepareCall(callUpdateInventoryStoredProcedure);
            stmt.setInt(1, partID);
            stmt.setInt(2, partAmount);

            int rowsUpdated = stmt.executeUpdate();
            success = rowsUpdated > 0;
        } catch (SQLException e) {
            e.printStackTrace(System.err);
            throw new RuntimeException(e);
        }

        return success;
    }
}