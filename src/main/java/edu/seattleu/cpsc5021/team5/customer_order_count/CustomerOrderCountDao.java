package edu.seattleu.cpsc5021.team5.customer_order_count;

import edu.seattleu.cpsc5021.team5.AppConfig;
import edu.seattleu.cpsc5021.team5.DbConfig;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

public class CustomerOrderCountDao {
    private static final String TOTAL_ORDERS_KEY = "sql.customer.orders";
    private Properties appConfig = AppConfig.getProperties();

    private DbConfig dbConfig = DbConfig.getInstance();

    public List<CustomerOrderCount> fetchAll() {
        String fetchAllPsuSql = appConfig.getProperty(TOTAL_ORDERS_KEY);

        List<CustomerOrderCount> orders = new LinkedList<>();

        try (
                Connection cxn = dbConfig.getDataSource().getConnection();
                ) {
            Statement stmt = cxn.createStatement();
            stmt.execute(fetchAllPsuSql);

            ResultSet resultSet = stmt.getResultSet();
            while (resultSet.next()) {
                String lastName = resultSet.getString(1);
                String firstName = resultSet.getString(2);
                Long orderCounts = resultSet.getLong(3);

                CustomerOrderCount order = new CustomerOrderCount(lastName, firstName, orderCounts);
                orders.add(order);
            }
        } catch (SQLException e) {
            e.printStackTrace(System.err);
            throw new RuntimeException(e);
        }

        return orders;
    }

}
