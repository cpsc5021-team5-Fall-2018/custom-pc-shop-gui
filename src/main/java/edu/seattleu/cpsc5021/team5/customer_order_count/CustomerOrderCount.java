package edu.seattleu.cpsc5021.team5.customer_order_count;

import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;

public class CustomerOrderCount {
    private SimpleStringProperty lastName;
    private SimpleStringProperty firstName;
    private SimpleLongProperty orderCounts;

    public CustomerOrderCount(String lastName, String firstName, Long orderCounts) {
        this.lastName = new SimpleStringProperty(lastName);
        this.firstName = new SimpleStringProperty(firstName);
        this.orderCounts = new SimpleLongProperty(orderCounts);
    }

    public String getLastName() {
        return lastName.get();
    }

    public SimpleStringProperty lastNameProperty() {
        return lastName;
    }

    public String getFirstName() {
        return firstName.get();
    }

    public SimpleStringProperty firstNameProperty() {
        return firstName;
    }

    public long getOrderCounts() {
        return orderCounts.get();
    }

    public SimpleLongProperty orderCountsProperty() {
        return orderCounts;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName()
                + " { lastName=" + lastName
                + ", firstName=" + firstName
                + ", totalOrders=" + orderCounts
                + " }";
    }
}


