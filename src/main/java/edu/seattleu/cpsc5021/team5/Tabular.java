package edu.seattleu.cpsc5021.team5;

import java.util.List;

public interface Tabular {
    /**
     * E.g. "Customer First Name", "Customer Last Name", "Order Count"
     * @return A list of human-readable labels for the columns in a controller's table UI.
     */
    List<String> header();

    /**
     * E.g. "Bob", "Johnson", "12"
     * @return A list of data elements that can be displayed in the table data fields.
     */
    List<Object> values();
}
