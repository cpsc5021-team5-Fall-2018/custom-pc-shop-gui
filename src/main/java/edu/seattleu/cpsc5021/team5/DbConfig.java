package edu.seattleu.cpsc5021.team5;


import com.mysql.cj.jdbc.MysqlConnectionPoolDataSource;
import com.mysql.cj.jdbc.MysqlDataSource;

import javax.sql.DataSource;
import java.util.Properties;

public class DbConfig {
    private static final String MYSQL_URL_KEY = "mysql.url";
    private static final String MYSQL_USER_KEY = "mysql.user";
    private static final String MYSQL_PASSWORD_KEY = "mysql.password";

    private DataSource dataSource;
    private Properties appConfig;

    private static DbConfig dbConfig;

    private DbConfig() {
        appConfig = AppConfig.getProperties();
    }

    public static DbConfig getInstance() {
        if (dbConfig == null)
            dbConfig = new DbConfig();

        return dbConfig;
    }

    public DataSource getDataSource() {
        if (dataSource == null)
            initializeDataSource();

        return dataSource;
    }

    private void initializeDataSource() {
        MysqlDataSource mysqlDataSource = new MysqlConnectionPoolDataSource();
        mysqlDataSource.setUrl(appConfig.getProperty(MYSQL_URL_KEY));
        mysqlDataSource.setUser(appConfig.getProperty(MYSQL_USER_KEY));
        mysqlDataSource.setPassword(appConfig.getProperty(MYSQL_PASSWORD_KEY));

        dataSource = mysqlDataSource;
    }
}
