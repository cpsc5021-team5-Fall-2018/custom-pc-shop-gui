package edu.seattleu.cpsc5021.team5.part.psu;

import edu.seattleu.cpsc5021.team5.AppConfig;
import edu.seattleu.cpsc5021.team5.DbConfig;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

public class PsuDao {
    private static final String FETCH_ALL_PSU_KEY = "sql.part.fetch-all-psu";
    private Properties appConfig = AppConfig.getProperties();

    private DbConfig dbConfig = DbConfig.getInstance();

    public List<PSU> fetchAll() {
        String fetchAllPsuSql = appConfig.getProperty(FETCH_ALL_PSU_KEY);

        List<PSU> psus = new LinkedList<>();

        try (
                Connection cxn = dbConfig.getDataSource().getConnection();
                ) {
            Statement stmt = cxn.createStatement();
            stmt.execute(fetchAllPsuSql);

            ResultSet resultSet = stmt.getResultSet();
            while (resultSet.next()) {
                String partName = resultSet.getString(1);
                String description = resultSet.getString(2);
                String serialNumber = resultSet.getString(3);
                Long unitPriceUsd = resultSet.getLong(4);
                Integer psuPowerRating = resultSet.getInt(5);

                PSU psu = new PSU(partName, description, serialNumber, unitPriceUsd, psuPowerRating);
                psus.add(psu);
            }
        } catch (SQLException e) {
            e.printStackTrace(System.err);
            throw new RuntimeException(e);
        }

        return psus;
    }

}
