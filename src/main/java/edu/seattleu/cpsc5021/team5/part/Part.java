package edu.seattleu.cpsc5021.team5.part;

import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;

public abstract class Part {
    private SimpleStringProperty partName;
    private SimpleStringProperty description;
    private SimpleStringProperty serialNumber;
    private SimpleLongProperty unitPriceUsd;

    public Part(String partName, String description, String serialNumber, Long unitPriceUsd) {
        this.partName = new SimpleStringProperty(partName);
        this.description = new SimpleStringProperty(description);
        this.serialNumber = new SimpleStringProperty(serialNumber);
        this.unitPriceUsd = new SimpleLongProperty(unitPriceUsd);
    }

    public String getPartName() {
        return partName.get();
    }

    public String getDescription() {
        return description.get();
    }

    public String getSerialNumber() {
        return serialNumber.get();
    }

    public Long getUnitPriceUsd() {
        return unitPriceUsd.get();
    }
}
