package edu.seattleu.cpsc5021.team5.part.build_item_detail;

import edu.seattleu.cpsc5021.team5.Tabular;
import edu.seattleu.cpsc5021.team5.part.Part;

import java.util.Arrays;
import java.util.List;

public class BuildItemDetail extends Part implements Tabular {
    private Integer itemQuantity;
    private Long totalPrice;

    public BuildItemDetail(String partName, String description, String serialNumber, Long unitPriceUsd, Integer itemQuantity, Long totalPrice) {
        super(partName, description, serialNumber, unitPriceUsd);
        this.itemQuantity = itemQuantity;
        this.totalPrice = totalPrice;
    }

    @Override
    public List<String> header() {
        return Arrays.asList(
                "Part Name",
                "Description",
                "Serial Number",
                "Unit Price (USD)",
                "Item Quantity",
                "Total Price"
        );
    }

    @Override
    public List<Object> values() {
        return Arrays.asList(
                getPartName(),
                getDescription(),
                getSerialNumber(),
                getUnitPriceUsd(),
                itemQuantity,
                totalPrice
        );
    }

    public Integer getItemQuantity() {
        return itemQuantity;
    }

    public Long getTotalPrice() {
        return totalPrice;
    }

    @Override
    public String toString() {
        return "BuildItemDetail { " +
            "partName=" + getPartName() +
            ", description=" + getDescription() +
            ", serialNumber=" + getSerialNumber() +
            ", unitPriceUsd=" + getUnitPriceUsd() +
            ", itemQuantity=" + itemQuantity +
            ", totalPrice=" + totalPrice +
        " }";
    }
}
