package edu.seattleu.cpsc5021.team5.part.psu;

import edu.seattleu.cpsc5021.team5.part.Part;
import javafx.beans.property.SimpleIntegerProperty;

public class PSU extends Part {
    private SimpleIntegerProperty psuPowerRating;

    public PSU(String partName, String description, String serialNumber, Long unitPriceUsd, Integer psuPowerRating) {
        super(partName, description, serialNumber, unitPriceUsd);
        this.psuPowerRating = new SimpleIntegerProperty(psuPowerRating);
    }

    public Integer getPsuPowerRating() {
        return psuPowerRating.get();
    }

    @Override
    public String toString() {
        return getClass().getSimpleName()
                + " { partName=" + getPartName()
                + ", description=" + getDescription()
                + ", serialNumber=" + getSerialNumber()
                + ", unitPriceUsd=" + getUnitPriceUsd()
                + ", psuPowerRating=" + getPsuPowerRating()
                + " }";
    }
}
