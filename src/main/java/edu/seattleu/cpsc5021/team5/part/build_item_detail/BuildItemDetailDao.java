package edu.seattleu.cpsc5021.team5.part.build_item_detail;

import edu.seattleu.cpsc5021.team5.AppConfig;
import edu.seattleu.cpsc5021.team5.DbConfig;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

public class BuildItemDetailDao {

    private static final String BUILD_BREAKDOWN = "sql.build.breakdown";

    private Properties appConfig = AppConfig.getProperties();

    private DbConfig dbConfig = DbConfig.getInstance();

    public List<BuildItemDetail> fetchByBuildId(Long buildId) {
            String fetchInventoryReorderSql = appConfig.getProperty(BUILD_BREAKDOWN);

            List<BuildItemDetail> results = new LinkedList<>();

            try (
                    Connection cxn = dbConfig.getDataSource().getConnection();
            ) {
                PreparedStatement stmt = cxn.prepareStatement(fetchInventoryReorderSql);
                stmt.setLong(1, buildId);

                ResultSet resultSet = stmt.executeQuery();
                while (resultSet.next()) {
                    String partName = resultSet.getString(1);
                    String description = resultSet.getString(2);
                    String serialNumber = resultSet.getString(3);
                    Long unitPriceUsd = resultSet.getLong(4);
                    Integer itemQuantity = resultSet.getInt(5);
                    Long totalPrice = resultSet.getLong(6);

                    BuildItemDetail partReorder = new BuildItemDetail(
                            partName,
                            description,
                            serialNumber,
                            unitPriceUsd,
                            itemQuantity,
                            totalPrice
                    );

                    results.add(partReorder);
                }

            } catch (SQLException e) {
                e.printStackTrace(System.err);
                throw new RuntimeException(e);
            }

        return results;
    }

}
