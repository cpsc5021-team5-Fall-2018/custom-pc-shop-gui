package edu.seattleu.cpsc5021.team5.part.part_reorder;

import edu.seattleu.cpsc5021.team5.Tabular;

import java.util.Arrays;
import java.util.List;

public class PartReorder implements Tabular {

    private Long partId;
    private String serialNumber;
    private Integer inventoryQuantity;
    private String partName;
    private String partType;
    private String vendorName;

    public PartReorder(
            Long partId,
            String serialNumber,
            Integer inventoryQuantity,
            String partName,
            String partType,
            String vendorName) {
        this.partId = partId;
        this.serialNumber = serialNumber;
        this.inventoryQuantity = inventoryQuantity;
        this.partName = partName;
        this.partType = partType;
        this.vendorName = vendorName;
    }

    @Override
    public List<String> header() {
        return Arrays.asList(
                "Part ID",
                "Serial Number",
                "Inventory Level",
                "Part Name",
                "Part Type",
                "Vendor Name"
        );
    }

    @Override
    public List<Object> values() {
        return Arrays.asList(
                partId,
                serialNumber,
                inventoryQuantity,
                partName,
                partType,
                vendorName
        );
    }

    public Long getPartId() {
        return partId;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public Integer getInventoryQuantity() {
        return inventoryQuantity;
    }

    public String getPartName() {
        return partName;
    }

    public String getPartType() {
        return partType;
    }

    public String getVendorName() {
        return vendorName;
    }

    @Override
    public String toString() {
        return "PartReorder { " +
        "partId=" + partId +
        ", serialNumber=" + serialNumber +
        ", inventoryQuantity=" + inventoryQuantity +
        ", partName=" + partName +
        ", partType=" + partType +
        ", vendorName=" + vendorName + " }";

    }
}
