package edu.seattleu.cpsc5021.team5.part.part_reorder;

import edu.seattleu.cpsc5021.team5.AppConfig;
import edu.seattleu.cpsc5021.team5.DbConfig;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

public class PartReorderDao {
    private static final String FETCH_INVENTORY_REORDER = "sql.inventory.reorder";
    private Properties appConfig = AppConfig.getProperties();

    private DbConfig dbConfig = DbConfig.getInstance();

    public List<PartReorder> fetchLessThanChokeLevel(Integer chokeLevel) {
        String fetchInventoryReorderSql = appConfig.getProperty(FETCH_INVENTORY_REORDER);

        List<PartReorder> results = new LinkedList<>();

        try (
                Connection cxn = dbConfig.getDataSource().getConnection();
        ) {
            PreparedStatement stmt = cxn.prepareStatement(fetchInventoryReorderSql);
            stmt.setInt(1, chokeLevel);

            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                Long partId = resultSet.getLong(1);
                String serialNumber = resultSet.getString(2);
                Integer inventoryQuantity = resultSet.getInt(3);
                String partName = resultSet.getString(4);
                String partType = resultSet.getString(5);
                String vendorName = resultSet.getString(6);

                PartReorder partReorder = new PartReorder(
                        partId,
                        serialNumber,
                        inventoryQuantity,
                        partName,
                        partType,
                        vendorName
                );

                results.add(partReorder);
            }

        } catch (SQLException e) {
            e.printStackTrace(System.err);
            throw new RuntimeException(e);
        }

        return results;
    }
}
