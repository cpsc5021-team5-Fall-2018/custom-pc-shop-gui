package edu.seattleu.cpsc5021.team5;

import java.util.List;
import java.util.stream.Collectors;

public class ControllerUtils {

    public static String toStringLines(List<?> items) {
        return items.stream()
                .map(String::valueOf)
                .collect(Collectors.joining("\n"));
    }

}