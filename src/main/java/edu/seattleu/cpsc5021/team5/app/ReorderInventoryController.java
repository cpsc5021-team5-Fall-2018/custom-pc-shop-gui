/*
 * Copyright (c) 2011, 2014 Oracle and/or its affiliates.
 * All rights reserved. Use is subject to license terms.
 *
 * This file is available and licensed under the following license:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *  - Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the distribution.
 *  - Neither the name of Oracle nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package edu.seattleu.cpsc5021.team5.app;

import edu.seattleu.cpsc5021.team5.AppConfig;
import edu.seattleu.cpsc5021.team5.part.part_reorder.PartReorder;
import edu.seattleu.cpsc5021.team5.part.part_reorder.PartReorderDao;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class ReorderInventoryController implements Initializable {
    private static final Integer MIN_CHOKE_LEVEL = 1;
    private static final String MIN_CHOKE_LEVEL_MESSAGE = String.format("Choke level must be at least %d!", MIN_CHOKE_LEVEL);
    private static final String INVALID_CHOKE_LEVEL_NUMBER_FORMAT = "Choke level must be a valid integer!";

    private PartReorderDao reorderDao = new PartReorderDao();

    @FXML private Text reorderInventoryQuery;
    @FXML private Text reorderInventoryErrorMessage;
    @FXML private TextField chokeInput;
    @FXML private TableView reorderInventoryTable;

    @FXML protected void handleSubmitButtonAction(ActionEvent event) {

        // clear UI state before doing anything
        clearChokeLevelErrorMessage();
        ObservableList<PartReorder> tableItems = reorderInventoryTable.getItems();
        tableItems.clear();

        try {
            Integer chokeLevel = extractChokeLevel();

            if (chokeLevel < MIN_CHOKE_LEVEL) {
                displayInvalidChokeLevelMessage();
            } else {
                List<PartReorder> partReorders = reorderDao.fetchLessThanChokeLevel(chokeLevel);
                if (partReorders.size() > 0)
                    partReorders.forEach(tableItems::add);
                else
                    noResults();
            }

        } catch (NumberFormatException e) {
            displayNumberFormatError();
        }

    }

    private Integer extractChokeLevel() {
        String chokeLevelStr = chokeInput.getText().trim();
        return Integer.parseInt(chokeLevelStr);
    }

    private void clearChokeLevelErrorMessage() {
        reorderInventoryErrorMessage.setText("");
    }

    private void displayNumberFormatError() {
        reorderInventoryErrorMessage.setText(INVALID_CHOKE_LEVEL_NUMBER_FORMAT);
    }

    private void displayInvalidChokeLevelMessage() {
        reorderInventoryErrorMessage.setText(MIN_CHOKE_LEVEL_MESSAGE);
    }

    private void noResults() {
        reorderInventoryErrorMessage.setText("Query executed; no results");
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        String reorderInventoryQueryText = AppConfig.getProperties().getProperty("sql.inventory.reorder");
        reorderInventoryQuery.setText(reorderInventoryQueryText);
    }
}
