package edu.seattleu.cpsc5021.team5.app;

import edu.seattleu.cpsc5021.team5.AppConfig;
import edu.seattleu.cpsc5021.team5.vendor.VendorDao;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ResourceBundle;

public class AddVendorController implements Initializable {
    private VendorDao addvendorDao = new VendorDao();

    @FXML private Text addVendorQuery;
    @FXML private TextField txtName;
    @FXML private Label addVendorLabel;
    @FXML private Text addVendorErrorText;

    @FXML protected void handleSubmitButtonAction(ActionEvent event) {
        clearOutput();

        String nameStr = txtName.getText().trim();

        if (nameStr.isEmpty()) {
            addVendorErrorText.setText("Vendor name cannot be empty!");
            return;
        }

        boolean addedVendor = addvendorDao.insertvendor(nameStr);
        if (addedVendor) {
            addVendorLabel.setText("Vendor Added");
        } else {
            addVendorErrorText.setText("Couldn't add vendor to the database (sorry)!");
        }

    }

    private void clearOutput() {
        addVendorLabel.setText("");
        addVendorErrorText.setText("");
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        String addVendoryQueryText = AppConfig.getProperties().getProperty("sql.vendor.add");
        addVendorQuery.setText(addVendoryQueryText);
    }
}