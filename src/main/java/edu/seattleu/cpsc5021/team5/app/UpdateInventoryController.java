package edu.seattleu.cpsc5021.team5.app;


import edu.seattleu.cpsc5021.team5.AppConfig;
import edu.seattleu.cpsc5021.team5.inventory.part_update.PartUpdateDao;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class UpdateInventoryController implements Initializable {
    private PartUpdateDao reorderDao = new PartUpdateDao();

    @FXML private Text updateInventoryQuery;

    @FXML private TextField txtPartID;
    @FXML private TextField txtSupplyAmount;
    @FXML private Label updateInventoryLabel;
    @FXML private Label updateInventoryStatus;

    @FXML protected void handleSubmitButtonAction(ActionEvent event) {
        String partIDStr = txtPartID.getText();
        String supplyAmountStr = txtSupplyAmount.getText();

        if (partIDStr.matches("\\d+") && supplyAmountStr.matches("\\d+")){
            updateInventoryStatus.setText("Result Status: Successful!");
        } else {
            updateInventoryStatus.setText("Result Status: Failed, Invalid Input!");
            updateInventoryLabel.setText("");
            return;
        }


        Integer partID = Optional.of(partIDStr).map(Integer::parseInt).orElse(3);
        Integer supplyAmount = Optional.of(supplyAmountStr).map(Integer::parseInt).orElse(3);

        boolean partUpdated = reorderDao.updateinventorysupply(partID,supplyAmount);
        String displayText;
        if (partUpdated){
            displayText = "Inventory updated";
        }
        else{
            displayText = "Couldn't update inventory";
        }

        updateInventoryLabel.setText(displayText);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        String updateInventoryQueryText = AppConfig.getProperties().getProperty("sql.inventory.part-update");
        updateInventoryQuery.setText(updateInventoryQueryText);
    }
}
