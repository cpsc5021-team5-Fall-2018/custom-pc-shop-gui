package edu.seattleu.cpsc5021.team5.app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class CustomPcShop extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("custom-pc-shop-app.fxml"));

        stage.setTitle("Custom PC Shop");
        stage.setScene(new Scene(root, 1024, 960));
        stage.show();
    }

}
