/*
 * Copyright (c) 2011, 2014 Oracle and/or its affiliates.
 * All rights reserved. Use is subject to license terms.
 *
 * This file is available and licensed under the following license:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *  - Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the distribution.
 *  - Neither the name of Oracle nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package edu.seattleu.cpsc5021.team5.app;

import edu.seattleu.cpsc5021.team5.AppConfig;
import edu.seattleu.cpsc5021.team5.part.build_item_detail.BuildItemDetail;
import edu.seattleu.cpsc5021.team5.part.build_item_detail.BuildItemDetailDao;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.collections.ObservableList;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;


import static edu.seattleu.cpsc5021.team5.ControllerUtils.toStringLines;

public class BuildBreakdownController implements Initializable {
    private BuildItemDetailDao buildBreakdownDao = new BuildItemDetailDao();

    @FXML private TextField configID;
    @FXML private TableView buildBreakDownTable;
    @FXML private Label buildBreakDownStatus;
    @FXML private Text buildBreakdownQuery;

    @FXML protected void handleSubmitButtonAction(ActionEvent event) {
        String configIDStr = configID.getText();

        if (!configIDStr.matches("\\d+")){
            buildBreakDownStatus.setText("Result Status: Failed, Invalid Input!");
            buildBreakDownTable.getItems().clear();
            return;
        }

        Long configIdNum = Long.parseLong(configIDStr);

        List<BuildItemDetail> buildItemDetails = buildBreakdownDao.fetchByBuildId(configIdNum);

        ObservableList<BuildItemDetail> tableItems = buildBreakDownTable.getItems();
        tableItems.clear();
        if(buildItemDetails.isEmpty()){
            buildBreakDownStatus.setText("Query executed; no results found.");
            return;
        }

        buildItemDetails.forEach(tableItems::add);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        String buildBreakdownQueryText = AppConfig.getProperties().getProperty("sql.build.breakdown");
        buildBreakdownQuery.setText(buildBreakdownQueryText);
    }
}
