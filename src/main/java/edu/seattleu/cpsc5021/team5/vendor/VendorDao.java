package edu.seattleu.cpsc5021.team5.vendor;

import edu.seattleu.cpsc5021.team5.AppConfig;
import edu.seattleu.cpsc5021.team5.DbConfig;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

public class VendorDao {
    private static final String INSERT_VENDOR_INTO_DB = "sql.vendor.add";
    private static final String MAX_VENDOR_ID = "sql.vendor.max-id";

    private Properties appConfig = AppConfig.getProperties();

    private DbConfig dbConfig = DbConfig.getInstance();

    private Integer computNextVendorId() {
        try (
                Connection cxn = dbConfig.getDataSource().getConnection();
        ) {
            String maxVendorQuery = appConfig.getProperty(MAX_VENDOR_ID);
            PreparedStatement stmt = cxn.prepareStatement(maxVendorQuery);
            ResultSet resultSet = stmt.executeQuery();

            Integer maxVendorId = resultSet.next()
                    ? resultSet.getInt(1)
                    : 0;

            return maxVendorId + 1;
        } catch (SQLException e) {
            e.printStackTrace(System.err);
            throw new RuntimeException(e);
        }
    }

    public boolean insertvendor(String name) {
        boolean success;

        Integer nextVendorId = computNextVendorId();

        try (
                Connection cxn = dbConfig.getDataSource().getConnection();
        ) {
            String insertVendor = appConfig.getProperty(INSERT_VENDOR_INTO_DB);
            PreparedStatement stmt = cxn.prepareStatement(insertVendor);
            stmt.setInt(1, nextVendorId);
            stmt.setString(2, name);

            int rowsUpdated = stmt.executeUpdate();
            success = rowsUpdated > 0;

        } catch (SQLException e) {
            e.printStackTrace(System.err);
            throw new RuntimeException(e);
        }

        return success;
    }
}
